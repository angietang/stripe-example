const express = require('express');
const router = express.Router();
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const paymentController = require('./paymentController')


router.get('/', (req, res) => {
  res.render('index.html');
});

router.get('/config', (req, res) => {
  res.send({
    config: {
      stripePublishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
    }
  })
})

router.route('/payment')
  .post(paymentController.createOrder);



module.exports = router; 