var stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

module.exports = {
  createOrder: (req, res) => {
    stripe.charges.create({
      amount: 999,
      currency: 'usd',
      description: 'Example charge',
      source: req.body.stripeToken,
    }, function (err, charge) {
      if (err) {
        console.log('error charging card', err);
        res.status(500).send({error: err});
      } else {
        console.log('charged card', charge);
        res.status(200).send(`Success! Your token was: ${req.body.stripeToken}`);
      }
    }) 
  }
}